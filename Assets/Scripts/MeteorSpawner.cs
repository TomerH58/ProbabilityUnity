using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MeteorSpawner : MonoBehaviour
{
    public GameObject _meteorPrefab;

    public float _spawnDistance = 10.0f;
    public float _speed = 0.1f;
    public float _labda = 2.0f;
    public TextMeshProUGUI _score;
    public RawImage _life1;
    public RawImage _life2;
    public RawImage _life3;
    public TextMeshProUGUI _gameOver;

    private RawImage[] _lives;
    private int _currentLives = 3;

    private float _timeToSpawn = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        _lives = new RawImage[] { _life3, _life2, _life1 };
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > _timeToSpawn)
        {
            _timeToSpawn = Time.time;
            float ex = GenerateExponentialValue();
            _timeToSpawn += ex;
            float angle = Random.value * 360;

            Vector2 vSpawn = new Vector2(_spawnDistance, 0.0f);

            float sin = Mathf.Sin(angle);
            float cos = Mathf.Cos(angle);

            float tx = vSpawn.x;
            float ty = vSpawn.y;
            vSpawn.x = (cos * tx) - (sin * ty);
            vSpawn.y = (sin * tx) + (cos * ty);

            GameObject meteor = Instantiate(_meteorPrefab, vSpawn, Quaternion.identity);
            LinearMovement meteorLinearMovement = meteor.GetComponent<LinearMovement>();
            BulletCollisionDetector bulletCollisionDetector = meteor.GetComponent<BulletCollisionDetector>();

            bulletCollisionDetector._score = _score;

            Vector2 vMove = -meteor.transform.position.normalized / (1 / _speed);
            meteorLinearMovement._movementX = vMove.x;
            meteorLinearMovement._movementY = vMove.y;
        }
    }

    public void HandleMeteorHit()
    {
        if(_currentLives > 0)
        {
            _currentLives -= 1;
            Destroy(_lives[_currentLives]);
        }

        if (_currentLives == 0)
        {
            _gameOver.gameObject.SetActive(true);
        }
    }

    public float GenerateExponentialValue()
    {
        float u = Random.value;
        float mean = 1 / _labda;

        return -mean * Mathf.Log(1 - u);

    }
}
