using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EarthCollisionDetector : MonoBehaviour
{
    private MeteorSpawner _meteorSpawner;

    // Start is called before the first frame update
    void Start()
    {
        _meteorSpawner = GameObject.Find("Gunpoint").GetComponent<MeteorSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Earth")
        {
            Destroy(gameObject);

            _meteorSpawner.HandleMeteorHit();
        }
    }
}
