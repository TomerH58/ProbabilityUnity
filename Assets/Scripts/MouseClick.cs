using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MouseClick : MonoBehaviour
{
    public GameObject _bulletPrefab;

    public float _mue = 0.0f;
    public float _sigma = 1.0f;
    public float _cooldown = 0.1f;
    private float _lastShot = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        //_score = GetComponent<TextMeshPro>();
        Random.InitState((int)System.DateTime.Now.Ticks);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Mouse0) && (Time.time - _lastShot) > _cooldown)
        {
            _lastShot = Time.time;
            Vector2 vector2 = Camera.main.ScreenToWorldPoint(Input.mousePosition).normalized;
            float theta = Mathf.Atan2(vector2.y, vector2.x);

            GameObject bullet = Instantiate(_bulletPrefab, Vector3.zero, Quaternion.identity);
            LinearMovement bulletLinearMovement = bullet.GetComponent<LinearMovement>();
            Vector2 vOriginal = new Vector2(1.0f, RandNormalDistribNum());

            float sin = Mathf.Sin(theta);
            float cos = Mathf.Cos(theta);

            float tx = vOriginal.x;
            float ty = vOriginal.y;
            vOriginal.x = (cos * tx) - (sin * ty);
            vOriginal.y = (sin * tx) + (cos * ty);

            bulletLinearMovement._movementX = vOriginal.x;
            bulletLinearMovement._movementY = vOriginal.y;
        }
    }

    private float RandNormalDistribNum()
    {
        // REJECTION METHOD

        // g(x) will be the uniform distribution - Random.value
        // f(x) is the desired normal distribution

        // the max of g(x) is 1 since g(x) = 1 (uniform)
        // therefor we choose c to be the max value of f(x)/g(x) = max value of f(x) = f(_mue) since the normal distribution is maxed at mue
        float uni = UniformDistrib();
        float c = NormalDistrib(_mue) / uni;

        float U1;
        float U2;
        int attempt = 1;
        // if U2 <= f(U1) / c*g(U1) return U1, Otherwise retry
        while (true)
        {
            // generate 2 numbers from g(x)
            U1 = Random.value * 40 * _sigma - (20 * _sigma) + _mue;
            U2 = Random.value;

            attempt++;

            if (U2 <= NormalDistrib(U1) / (c * uni))
            {
                return U1;
            }
        }
    }

    private float NormalDistrib(float x)
    {
        return (1 / _sigma * Mathf.Sqrt(Mathf.PI * 2)) * Mathf.Exp((-Mathf.Pow(x - _mue, 2)) / (2 * Mathf.Pow(_sigma, 2)));
    }

    private float UniformDistrib()
    {
        return 1 / (40 * _sigma);
    }
}
