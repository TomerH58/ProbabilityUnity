using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinearMovement : MonoBehaviour
{
    Rigidbody2D _rigidBody2D;

    public float _movementX;
    public float _movementY;
    public float _limit;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.magnitude > _limit)
        {
            Destroy(gameObject);
        }

        _rigidBody2D.AddForce(new Vector2(_movementX, _movementY));
    }
}
